# SPDX-FileCopyrightText: 2022 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends Node3D

@onready var timer := get_node("Timer")
@onready var world := get_parent().get_parent()

func _ready():
	timer.start(randf_range(Globals.spawner_min_interval,
		Globals.spawner_max_interval))

func spawn_phantom() -> void:
	world.spawn_phantom(global_position)
	timer.start(randf_range(Globals.spawner_min_interval,
		Globals.spawner_max_interval))
