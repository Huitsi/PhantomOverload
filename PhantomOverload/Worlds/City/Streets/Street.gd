# SPDX-FileCopyrightText: 2022 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

@onready var city := get_parent()

func generate_adjacent() -> void:
	for direction in [Vector3(1, 0, 0), Vector3(-1, 0, 0)]:
		city.create_adjacent_section(self, direction, city.choose_block_scene())
	for direction in [Vector3(0, 0, 1), Vector3(0, 0, -1)]:
		city.create_adjacent_section(self, direction, city.INTERSECTION_SCENE)
