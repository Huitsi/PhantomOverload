# SPDX-FileCopyrightText: 2022 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

const BLOCK_WIDTH := 95.0
const MIN_BUILDING_WIDTH = 10.0
const MIN_BUILDING_HEIGHT = 3.0
const MAX_BUILDING_HEIGHT = 100.0
const BUILDING_SCENE := preload("res://Worlds/City/CityBlock/Building/Building.tscn")

func _ready():
	var x_widths = []
	var width_left = BLOCK_WIDTH
	while (width_left > MIN_BUILDING_WIDTH):
		var w = randf_range(0, width_left)
		x_widths.append(w)
		width_left -= w
	x_widths.append(width_left)
	x_widths.shuffle()

	var z_widths = []
	width_left = BLOCK_WIDTH
	while (width_left > MIN_BUILDING_WIDTH):
		var w = randf_range(0, width_left)
		z_widths.append(w)
		width_left -= w
	z_widths.append(width_left)
	z_widths.shuffle()

	var x = - BLOCK_WIDTH / 2
	for x_width in x_widths:
		var z = - BLOCK_WIDTH / 2
		x += x_width / 2
		for z_width in z_widths:
			z += z_width / 2
			if x_width >= MIN_BUILDING_WIDTH and z_width >= MIN_BUILDING_WIDTH:
				var b = BUILDING_SCENE.instantiate()
				b.position = Vector3(x, 0, z)
				add_child(b)
				b.build(Vector3(x_width, randf_range(MIN_BUILDING_HEIGHT,
					MAX_BUILDING_HEIGHT), z_width))
			z += z_width / 2
		x += x_width / 2
