# SPDX-FileCopyrightText: 2022 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends Node3D

const WALL_THICKNESS := 0.5
const MIN_WINDOW_WIDTH := 2.1
const LIT_ROOM_CHANCE := 0.5
const DIRECTIONS := [Vector3.LEFT, Vector3.FORWARD, Vector3.RIGHT, Vector3.BACK]

@onready var collision := get_node("CollisionShape3D")
@onready var shadow := get_node("ShadowCaster")
@onready var pillars: MultiMesh = get_node("Pillars").multimesh
@onready var floors: MultiMesh = get_node("Floors").multimesh
@onready var lit_rooms: MultiMesh = get_node("LitRooms").multimesh
@onready var dark_rooms: MultiMesh = get_node("DarkRooms").multimesh

func build(dimensions: Vector3):
	collision.position.y = dimensions[1] / 2
	collision.shape.size = dimensions
	shadow.position.y = dimensions[1] / 2
	shadow.mesh.size = dimensions

	var window_counts := [0,0,0]
	var window_separations := Vector3()
	var floor_separations := Vector3()
	for i in [0, 1, 2]:
		window_counts[i] = int(randf_range(2, dimensions[i]/MIN_WINDOW_WIDTH))
		window_separations[i] = (dimensions[i] - WALL_THICKNESS) / window_counts[i]
		floor_separations[i] = dimensions[i] / window_counts[i]

	# Pillars

	pillars.mesh.size.y = dimensions[1]
	pillars.instance_count = 2 * (window_counts[0] + window_counts[2])
	var pillar_i := 0
	var pillar_pos := 0.5 * Vector3(dimensions[0] - WALL_THICKNESS,
			dimensions[1], dimensions[2] - WALL_THICKNESS)
	for direction in DIRECTIONS:
		var pstep :=  Vector3(direction[0] * window_separations[0], 0,
			direction[2] * window_separations[2])
		var pcount := absi(window_counts[0] * direction[0]
			+ window_counts[2] * direction[2])
		for __ in range(0, pcount):
			pillars.set_instance_transform(pillar_i,
				Transform3D.IDENTITY.translated(pillar_pos))
			pillar_i += 1
			pillar_pos += pstep

	# Floors

	floors.mesh.size.x = dimensions[0]
	floors.mesh.size.z = dimensions[2]
	floors.instance_count = window_counts[1] + 1
	var floor_translate := Vector3(0, floor_separations[1], 0)
	for i in range(0, floors.instance_count):
		floors.set_instance_transform(i,
			Transform3D.IDENTITY.translated(i * floor_translate))

	# Windows

	var room_count: int = 2 * (window_counts[0] * window_counts[1] +
		(window_counts[2] - 2) * window_counts[1])
	var room_lit_statuses := []
	var lit_room_count := 0
	var dark_room_count := 0
	for __ in range(0, room_count):
		var lit := randf() < LIT_ROOM_CHANCE
		room_lit_statuses.push_back(lit)
		if lit:
			lit_room_count += 1
		else:
			dark_room_count += 1

	lit_rooms.mesh.size = window_separations
	dark_rooms.mesh.size = window_separations
	lit_rooms.instance_count = lit_room_count
	dark_rooms.instance_count = dark_room_count

	var lit_room_i := 0
	var dark_room_i := 0
	var room_stack_pos := 0.5 * Vector3(
		dimensions.x - window_separations.x - WALL_THICKNESS,
		window_separations.y + WALL_THICKNESS,
		dimensions.z - window_separations.z - WALL_THICKNESS)
	for direction in DIRECTIONS:
		var rstep: Vector3 = direction * window_separations
		var rcount := absi(window_counts[0] * direction[0]
			+ window_counts[2] * direction[2]) - 1
		for __ in range(0, rcount):
			var room_pos := room_stack_pos
			for ___ in window_counts[1]:
				if room_lit_statuses.pop_back():
					lit_rooms.set_instance_transform(lit_room_i,
						Transform3D.IDENTITY.translated(room_pos))
					lit_room_i += 1
				else:
					dark_rooms.set_instance_transform(dark_room_i,
						Transform3D.IDENTITY.translated(room_pos))
					dark_room_i += 1
				room_pos.y += window_separations.y
			room_stack_pos += rstep
