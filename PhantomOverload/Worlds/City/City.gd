# SPDX-FileCopyrightText: 2022 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends World

const DTN := 55.5
const BLOCK_SCENE := preload("res://Worlds/City/CityBlock/CityBlock.tscn")
const PARK_SCENE := preload("res://Worlds/City/Park/Park.tscn")
const STREET_SCENE := preload("res://Worlds/City/Streets/Street.tscn")
const INTERSECTION_SCENE := preload("res://Worlds/City/Streets/Intersection.tscn")

@onready var sections := {"0,0": get_node("Intersection")}

func create_adjacent_section\
		(caller: Node3D, direction: Vector3, scene: PackedScene) -> Node:
	var adj_position = caller.to_global(DTN * direction)
	var key = str(int(round(adj_position.x/DTN)),",",int(round(adj_position.z/DTN)))
	if sections.has(key) and is_instance_valid(sections[key]): return null
	var section = scene.instantiate()
	section.position = adj_position
	sections[key] = section
	add_child(section)
	return section

func choose_block_scene() -> PackedScene:
	var rn := randf()
	if rn < 0.1:
		return PARK_SCENE
	return BLOCK_SCENE
