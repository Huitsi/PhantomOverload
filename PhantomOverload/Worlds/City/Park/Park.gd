# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

func _ready() -> void:
	for x in Vector2(-46, 47):
		for z in Vector2(-46, 47):
			var prop
			var rn = randf()
			if (rn < .003):
				prop = preload("res://Props/ScotsPine/ScotsPine.tscn").instantiate()
			elif (rn < .005):
				prop = preload("res://Props/NorwaySpruce/NorwaySpruce.tscn").instantiate()
			elif (rn < .006):
				prop = preload("res://Props/Lamppost/Lamppost.tscn").instantiate()
			elif (rn < .00605):
				prop = preload("res://Props/Statue/Statue.tscn").instantiate()
			if prop:
				prop.position = Vector3(x, position.y, z)
				add_child(prop)
