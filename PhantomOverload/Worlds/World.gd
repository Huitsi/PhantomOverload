# SPDX-FileCopyrightText: 2022-2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
class_name World extends Node3D

const MAX_SPAWN_DISTANCE_SQUARED := 100.0 ** 2
const MIN_SPAWN_DISTANCE_SQUARED := 50.0 ** 2
const PHANTOM_SCENE := preload("res://Phantom/Phantom.tscn")

@onready var phantoms := $Phantoms
@onready var player: Node3D = $Player

func spawn_phantom(spawn_position: Vector3) -> void:
	if phantoms.get_child_count() >= Globals.phantom_limit:
		return
	var distance_squared_to_player = spawn_position.distance_squared_to(player.position)
	if  distance_squared_to_player > MAX_SPAWN_DISTANCE_SQUARED or\
		distance_squared_to_player < MIN_SPAWN_DISTANCE_SQUARED:
			return
	var phantom = PHANTOM_SCENE.instantiate()
	phantom.position = spawn_position
	phantoms.add_child(phantom)
