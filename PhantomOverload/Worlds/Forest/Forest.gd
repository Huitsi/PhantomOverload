# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends World

const SECTION_INTERVAL := 100.0
const HECTARE_SCENE := preload("res://Worlds/Forest/Hectare/Hectare.tscn")
@onready var sections := {Vector3.ZERO: get_node("Hectare")}

func generate_sections_adjacent_to(center_section: Node3D) -> void:
	for direction: Vector3 in [Vector3(1, 0, 0), Vector3(-1, 0, 0), Vector3(0, 0, 1), Vector3(0, 0, -1)]:
		var section_position := center_section.to_global(SECTION_INTERVAL * direction).round()
		if sections.has(section_position) and is_instance_valid(sections[section_position]):
			continue
		var section := HECTARE_SCENE.instantiate()
		section.position = section_position
		sections[section_position] = section
		add_child(section)
