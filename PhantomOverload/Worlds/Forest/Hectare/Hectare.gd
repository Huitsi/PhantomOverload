# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

const EDGE_VERTEX_COUNT := 101
const TERRAIN_AMPLITUDE := 3.5
const VERTEX_OFFSET := Vector3(-50, 0, -50)

static var noise := FastNoiseLite.new()
@onready var world := get_parent()
@onready var meshi := get_node("GroundMesh")
@onready var heightmap: HeightMapShape3D = get_node("GroundShape").shape

func _ready():
	heightmap.map_width = EDGE_VERTEX_COUNT
	heightmap.map_depth = EDGE_VERTEX_COUNT

	var height_array := heightmap.map_data
	for z in range(0, EDGE_VERTEX_COUNT):
		for x in range(0, EDGE_VERTEX_COUNT):
			var height := noise.get_noise_2d(x + position.x, z + position.z) * TERRAIN_AMPLITUDE
			height_array.set(x + z * EDGE_VERTEX_COUNT, height)
	heightmap.map_data = height_array

	var mb := MeshBuilder.new()
	for z in range(0, EDGE_VERTEX_COUNT - 1):
		for x in range(0, EDGE_VERTEX_COUNT - 1):
			var v1 := Vector3(x + 1, height_array[(x + 1) + z * EDGE_VERTEX_COUNT], z) + VERTEX_OFFSET
			var v2 := Vector3(x + 1, height_array[(x + 1) + (z + 1) * EDGE_VERTEX_COUNT], z + 1) + VERTEX_OFFSET
			var v3 := Vector3(x, height_array[x + (z + 1) * EDGE_VERTEX_COUNT], z + 1) + VERTEX_OFFSET
			var v4 := Vector3(x, height_array[x + z * EDGE_VERTEX_COUNT], z) + VERTEX_OFFSET
			mb.add_quadrilateral(v1, v2, v3, v4)

			var prop = null
			var rn = randf()
			if (rn < .004):
				prop = preload("res://Props/ScotsPine/ScotsPine.tscn").instantiate()
			elif (rn < .005):
				prop = preload("res://Props/NorwaySpruce/NorwaySpruce.tscn").instantiate()
			elif (rn < .00525):
				prop = preload("res://PhantomSpawner/PhantomSpawner.tscn").instantiate()
			elif (rn < .005251):
				prop = preload("res://Props/Lamppost/Lamppost.tscn").instantiate()
			if prop:
				prop.position = Vector3(x, height_array[x + z * EDGE_VERTEX_COUNT], z) + VERTEX_OFFSET
				add_child(prop)
	meshi.mesh = mb.commit_to_mesh()


func generate_adjacent() -> void:
	world.generate_sections_adjacent_to(self)
