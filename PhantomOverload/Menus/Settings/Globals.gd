# SPDX-FileCopyrightText: 2022 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends Node

signal fov_changed
signal tonemap_exposure_changed

var fov := 75.0:
	set(value):
		fov = value
		fov_changed.emit()

var tonemap_exposure := 1.0:
	set(value):
		tonemap_exposure = value
		tonemap_exposure_changed.emit()

var viewbob_intensity := 1.0

var phantom_transparency := 0.75:
	set(value):
		phantom_transparency = value
		RenderingServer.global_shader_parameter_set("phantom_transparency", value)

var phantom_limit: int = ProjectSettings.get("defaults/spawning/phantom_limit")

var spawner_max_interval: float = \
	ProjectSettings.get("defaults/spawning/max_interval")
var spawner_min_interval: float = \
	ProjectSettings.get("defaults/spawning/min_interval")

var immortal := false
