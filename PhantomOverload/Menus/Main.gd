# SPDX-FileCopyrightText: 2022-2025 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends Node

var current_world: Node3D

@onready var menu := get_node("Menu")
@onready var label := get_node("Menu/Label")

@onready var resume_button := get_node("Menu/ResumeButton")
@onready var load_city_button := get_node("Menu/LoadCityButton")
@onready var settings_button := get_node("Menu/SettingsButton")
@onready var licenses_button := get_node("Menu/LicensesButton")

@onready var settings := get_node("MarginContainer/Settings")
@onready var licenses := get_node("MarginContainer/Licenses")
@onready var time_counter := get_node("TimeCounter")
@onready var kill_counter := get_node("KillCounter")

func  _ready():
	Godopt.add_meter_to_display("phantom_count", get_phantom_count)
	var extra_settings =\
	{
		"display":
		{
			"tonemap_exposure": SliderSetting.new(func(value): Globals.tonemap_exposure = value, func(): return Globals.tonemap_exposure, 0.1, 16.0),
		},
		"gameplay":
		{
			"fov": SliderSetting.new(func(value): Globals.fov = value, func(): return Globals.fov, 45, 135, 1, 0),
			"viewbob_intensity": SliderSetting.new(func(value): Globals.viewbob_intensity = value, func(): return Globals.viewbob_intensity, 0.0, 3.0),
			"phantom_limit": IntegerSetting.new(func(value): Globals.phantom_limit = value, func(): return Globals.phantom_limit),
			"phantom_transparency": SliderSetting.new(func(value): Globals.phantom_transparency = value, func(): return Globals.phantom_transparency, 0, 1, 0.25, 2),
			"show_time_counter": ToggleSetting.new(time_counter.set_visible, time_counter.is_visible),
			"show_kill_counter": ToggleSetting.new(kill_counter.set_visible, kill_counter.is_visible),
		},
		"debug":
		{
			"immortal": ToggleSetting.new(func(value): Globals.immortal = value, func(): return Globals.immortal),
		}
	}
	settings.add_settings(extra_settings)
	load_city_button.grab_focus()

func pause() -> void:
	get_tree().paused = true
	if resume_button.visible:
		resume_button.grab_focus()
	else:
		load_city_button.grab_focus()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	menu.show()

func unpause() -> void:
	menu.hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	get_tree().paused = false

func show_death_screen() -> void:
	resume_button.hide()
	label.text = "Dead\nTime survived: " + time_counter.text + "\nPhantoms killed: " + kill_counter.text
	time_counter.text = ""
	kill_counter.text = ""
	pause()

func load_world(path: String) -> void:
	if is_instance_valid(current_world):
		current_world.free()
	current_world = load(path).instantiate()
	add_child(current_world)
	unpause()
	resume_button.show()
	label.text = "Paused"

func open_settings() -> void:
	menu.hide()
	settings.show()

func close_settings() -> void:
	settings.hide()
	settings_button.grab_focus()
	menu.show()

func open_licenses() -> void:
	menu.hide()
	licenses.show()

func close_licenses() -> void:
	licenses.hide()
	licenses_button.grab_focus()
	menu.show()

func quit() -> void:
	get_tree().quit()

func get_phantom_count() -> int:
	if !is_instance_valid(current_world):
		return 0
	return current_world.phantoms.get_child_count()
