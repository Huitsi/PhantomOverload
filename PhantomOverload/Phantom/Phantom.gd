# SPDX-FileCopyrightText: 2022-2025 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends CharacterBody3D

const GRAVITY:= Vector3(0, -9.8, 0)
const DESPAWN_DISTANCE_SQUARED := 110.0 ** 2

var speed := randf_range(5, 5.2)
var target_offset := Vector3(randf() * 0.5, 0, 0).rotated(Vector3.UP, randf() * TAU)

@onready var world: Node3D = get_parent().get_parent()
@onready var player: Node3D = world.get_node("Player")
@onready var camera: Node3D = world.get_node("Player/Camera3D")

@onready var eyes: Node3D = get_node("Eyes")

func _physics_process(delta):
	eyes.look_at(camera.global_position)

	if player.dead:
		return

	if position.distance_squared_to(player.position) > DESPAWN_DISTANCE_SQUARED:
		queue_free()

	if is_on_floor():
		velocity = speed * position.direction_to(player.position + target_offset)

	velocity += GRAVITY * delta

	move_and_slide()

func get_rekt() -> bool:
	queue_free()
	return true
