# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends MeshInstance3D

func _ready():
	var curve: Array[Vector2] =\
	[
		Vector2.ZERO,
		Vector2(0.19, 1.41),
		Vector2(0.08, 1.46),
		Vector2(0.06, 1.53),
		Vector2(0.08, 1.58),
		Vector2(0.08, 1.7),
		Vector2(0.06, 1.75),
		Vector2(0, 1.77)
	]
	var mb = MeshBuilder.new()
	mb.add_vertical_surface_of_revolution(curve)
	mesh = mb.commit()
