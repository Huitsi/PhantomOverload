# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
class_name MeshBuilder extends RefCounted

var st = SurfaceTool.new()

func _init() -> void:
	st.begin(Mesh.PRIMITIVE_TRIANGLES)

func  add_vertex(v: Vector3):
	st.set_smooth_group(-1)
	st.add_vertex(v)

func  add_vertex_with_uv(v: Vector3, uv: Vector2):
	st.set_smooth_group(-1)
	st.set_uv(uv)
	st.add_vertex(v)

func add_triangle(v1: Vector3, v2: Vector3, v3: Vector3) -> void:
	add_vertex(v1)
	add_vertex(v2)
	add_vertex(v3)

func add_triangle_with_uvs(v1: Vector3, v2: Vector3, v3: Vector3, uv1: Vector2, uv2: Vector2, uv3: Vector2) -> void:
	add_vertex_with_uv(v1, uv1)
	add_vertex_with_uv(v2, uv2)
	add_vertex_with_uv(v3, uv3)

func add_quadrilateral(v1: Vector3, v2: Vector3, v3: Vector3, v4: Vector3) -> void:
	add_triangle(v1, v2, v3)
	add_triangle(v3, v4, v1)

func add_quadrilateral_with_uvs(v1: Vector3, v2: Vector3, v3: Vector3, v4: Vector3,
		uv1: Vector2, uv2: Vector2, uv3: Vector2, uv4: Vector2) -> void:
	add_triangle_with_uvs(v1, v2, v3, uv1, uv2, uv3)
	add_triangle_with_uvs(v3, v4, v1, uv3, uv4, uv1)

func add_quadrilateral_based_pyramid(base_v1: Vector3, base_v2: Vector3, base_v3: Vector3, base_v4: Vector3, apex: Vector3):
	add_quadrilateral(base_v1, base_v2, base_v3, base_v4)
	add_triangle(base_v1, apex, base_v2)
	add_triangle(base_v2, apex, base_v3)
	add_triangle(base_v3, apex, base_v4)
	add_triangle(base_v4, apex, base_v1)

func add_box(dimensions: Vector3, position := Vector3.ZERO):
	var lbn := position
	var rbn := position + Vector3(dimensions.x, 0, 0)
	var ltn := position + Vector3(0, dimensions.y, 0)
	var rtn := position + Vector3(dimensions.x, dimensions.y, 0)
	var lbf := position + Vector3(0, 0, dimensions.z)
	var rbf := position + Vector3(dimensions.x, 0, dimensions.z)
	var ltf := position + Vector3(0, dimensions.y, dimensions.z)
	var rtf := position + dimensions

	# Near
	add_quadrilateral(lbn, rbn, rtn, ltn)
	# Far
	add_quadrilateral(lbf, ltf, rtf, rbf)
	# Left
	add_quadrilateral(lbn, ltn, ltf, lbf)
	# Right
	add_quadrilateral(rbn, rbf, rtf, rtn)
	# Bottom
	add_quadrilateral(lbn, lbf, rbf, rbn)
	# Top
	add_quadrilateral(ltn, rtn, rtf, ltf)

func add_vertical_surface_of_revolution(curve: Array[Vector2], set_uvs := false, radial_segments := 8) -> void:
	var curve_length := 0.0
	var segment_lengths: Array[float] = []
	if set_uvs:
		for point_index in range(0, curve.size() - 1):
			var point := curve[point_index]
			var next_point := curve[point_index + 1]
			var length := point.distance_to(next_point)
			segment_lengths.append(length)
			curve_length += length

	var rel_distance: float
	var next_rel_distance := 0.0
	var angle_increment := TAU / radial_segments
	for point_index in range(0, curve.size() - 1):
		var point := curve[point_index]
		var next_point := curve[point_index + 1]
		if set_uvs:
			rel_distance = next_rel_distance
			next_rel_distance = rel_distance + segment_lengths[point_index] / curve_length
		for angle in Vector3(0, TAU, angle_increment):
			var next_angle := angle - angle_increment
			var x1 := cos(angle)
			var z1 := sin(angle)
			var x2 := cos(next_angle)
			var z2 := sin(next_angle)

			var v1 := Vector3(point.x * x1, point.y, point.x * z1)
			var v2 := Vector3(next_point.x * x1, next_point.y, next_point.x * z1)
			var v3 := Vector3(next_point.x * x2, next_point.y, next_point.x * z2)
			var v4 := Vector3(point.x * x2, point.y, point.x * z2)

			if set_uvs:
				var rel_angle := angle / TAU
				var next_rel_angle := next_angle / TAU
				var uv1 := Vector2(rel_angle, rel_distance)
				var uv2 := Vector2(rel_angle, next_rel_distance)
				var uv3 := Vector2(next_rel_angle, next_rel_distance)
				var uv4 := Vector2(next_rel_angle, rel_distance)
				add_quadrilateral_with_uvs(v1, v2, v3, v4, uv1, uv2, uv3, uv4)
			else:
				add_quadrilateral(v1, v2, v3, v4)

func finalize() -> void:
	st.generate_normals()

func commit_to_arrays() -> Array:
	finalize()
	return st.commit_to_arrays()

func commit_to_mesh() -> ArrayMesh:
	finalize()
	return st.commit()

func commit(existing: ArrayMesh = null) -> ArrayMesh:
	finalize()
	return st.commit(existing)

func clear() -> void:
	st.clear()
	_init()
