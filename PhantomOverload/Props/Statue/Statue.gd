# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

func _ready():
	var size := randf_range(1, 3)
	scale = Vector3(size, size, size)
