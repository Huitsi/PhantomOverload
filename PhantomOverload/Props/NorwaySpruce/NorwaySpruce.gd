# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

const MAX_RADIUS := 0.85
const MIN_RADIUS := 0.01
const MAX_HEIGHT := 45.0
const MIN_HEIGHT := 0.5
const MAX_DEPTH := 3.0
const MIN_DEPTH := 0.5

func _ready() -> void:
	var size = randf()
	var radius := lerpf(MIN_RADIUS, MAX_RADIUS, size)
	var height := lerpf(MIN_HEIGHT, MAX_HEIGHT, size)
	var depth := lerpf(MIN_DEPTH, MAX_DEPTH, size)

	var shape_node := $CollisionShape3D
	shape_node.position.y = (height - depth) / 2
	var shape: CapsuleShape3D = shape_node.shape
	shape.radius = radius
	shape.height = height + depth

	var mb := MeshBuilder.new()
	mb.add_vertical_surface_of_revolution([Vector2(0, -depth), Vector2(6 * radius, -depth / 2), Vector2(radius, 0), Vector2(0, height)])
	var mesh := mb.commit()
	mesh.surface_set_material(0, preload("res://Props/NorwaySpruce/TrunkMaterial.tres"))

	mb.clear()
	var branch_count = randi_range(4, 8)
	var branch_start = height / 3
	var branch_height = (height - branch_start) / branch_count
	var branch_radius := 15 * radius
	var curve: Array[Vector2] = [Vector2(0, branch_start)];
	for branch in range(0, branch_count):
		var branch_end = branch_start + branch_height
		curve.append(Vector2(branch_radius, branch_start))
		curve.append(Vector2(0, branch_end))
		branch_start = branch_end
		branch_radius *= 0.8
	mb.add_vertical_surface_of_revolution(curve, true)
	mb.commit(mesh)
	mesh.surface_set_material(1, preload("res://Props/NorwaySpruce/FoliageMaterial.tres"))

	$MeshInstance3D.mesh = mesh
