# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

func get_rekt():
	$LampMesh.queue_free()
	$Light.queue_free()
	var audio: AudioStreamPlayer3D = $BreakAudio
	audio.finished.connect(queue_free)
	audio.play()
