# SPDX-FileCopyrightText: 2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends ArrayMesh

func _init():
	var mb := MeshBuilder.new()
	mb.add_box(Vector3(0.2, 2, 0.2), Vector3(-0.1, -1, -0.1))
	mb.add_vertical_surface_of_revolution([Vector2(0.05, 1), Vector2(0.05, 3)])
	mb.add_box(Vector3(0.4, 0.05, 0.4), Vector3(-0.2, 3, -0.2))

	mb.add_box(Vector3(0.05, 0.5, 0.05), Vector3(-0.2, 3.05, -0.2))
	mb.add_box(Vector3(0.05, 0.5, 0.05), Vector3(+0.15, 3.05, -0.2))
	mb.add_box(Vector3(0.05, 0.5, 0.05), Vector3(-0.2, 3.05, +0.15))
	mb.add_box(Vector3(0.05, 0.5, 0.05), Vector3(+0.15, 3.05, +0.15))

	mb.add_quadrilateral_based_pyramid(Vector3(-0.2, 3.55, -0.2), Vector3(-0.2, 3.55, 0.2), Vector3(0.2, 3.55, 0.2), Vector3(0.2, 3.55, -0.2), Vector3(0, 4, 0))
	add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, mb.commit_to_arrays())
