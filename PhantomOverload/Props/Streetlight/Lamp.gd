# SPDX-FileCopyrightText: 2022 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends StaticBody3D

@export var dark_probability := 0.0
@export var flickering_probability := 0.0
const FLICKER_MAX_TIME := 1.0
const DARK_MATERIAL = preload("res://Worlds/City/CityBlock/Building/DarkWindowMaterial.tres")

@onready var light := get_node("Light")
@onready var mesh := get_node("LampMesh")
@onready var timer := get_node("Timer")
@onready var flicker_audio := get_node("FlickerAudio")
@onready var break_audio := get_node("BreakAudio")

func _ready():
	var r = randf()
	if r < dark_probability:
		mesh.material_override = DARK_MATERIAL
		light.queue_free()
	elif r > (1.0 - flickering_probability):
		timer.start(randf() * FLICKER_MAX_TIME)

func flicker():
	light.visible = !light.visible
	if light.visible:
		mesh.material_override = null
		flicker_audio.play()
	else:
		mesh.material_override = DARK_MATERIAL
	timer.start(randf() * FLICKER_MAX_TIME)

func get_rekt():
	if is_instance_valid(light):
		mesh.material_override = DARK_MATERIAL
		break_audio.play()
		light.queue_free()
		timer.stop()
