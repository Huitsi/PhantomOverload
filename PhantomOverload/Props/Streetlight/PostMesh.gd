# SPDX-FileCopyrightText: 2022-2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends ArrayMesh

func _init():
	var mb := MeshBuilder.new()
	mb.add_vertical_surface_of_revolution([Vector2(0.1, 0), Vector2(0.1, 1), Vector2(0.05, 1.1), Vector2(0.05, 6)])
	add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, mb.commit_to_arrays())
