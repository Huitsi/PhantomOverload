# SPDX-FileCopyrightText: 2022-2024 Linus Vanas
# SPDX-License-Identifier: GPL-3.0-or-later
extends CharacterBody3D

const MOUSE_X_SENSITIVITY := -0.005
const MOUSE_Y_SENSITIVITY := -0.005

const STICK_X_SENSITIVITY := -0.035
const STICK_Y_SENSITIVITY := -0.035

const WALK_SPEED := 5.0
const GRAVITY:= Vector3(0, -9.8, 0)

const PHANTOM_MAX_DISTANCE_SQ := 9.55 ** 2
const PHANTOM_MIN_DISTANCE_SQ := 0.55 ** 2

const STEP_MAX_PROGRESS := 10.0
const VIEWBOB_BASE_AMPLITUDE := 0.05
const MAX_SILENT_FALL_TIME := 0.2

const DEATH_BRIGHTNESS := 1.5
const RAPTURE_SPEED := 1.4
const RAPTURE_PITCH_SPEED := 0.02
const RAPTURE_TIME := 6.5
const STARE_TIME := 1.5

var dead := false
var gun_raised := true
var rapture_started = false
var step_phase := 0.0
var fall_time := 0.0
var time_count := 0.0
var kill_count := 0

@onready var yaw_node := self
@onready var pitch_node := get_node("Camera3D")
@onready var camera := pitch_node
@onready var camera_height: float = camera.position.y

@onready var shadow_body := get_node("Body/ShadowCaster")
@onready var shadow_arms := get_node("Arms")
@onready var gun := get_node("Camera3D/Gun")
@onready var gunray := get_node("Camera3D/Gun/RayCast3D")
@onready var gun_animation := get_node("Camera3D/Gun/AnimationPlayer")
@onready var gun_audio := get_node("Camera3D/Gun/Audio")
@onready var gun_area := get_node("Camera3D/GunArea")
@onready var phantom_effect_area := get_node("PhantomEffectArea")
@onready var ambient_noise: AudioStreamPlayer = get_node("AmbientNoise")
@onready var footstep_audio: AudioStreamPlayer3D = get_node("FootstepAudio")
@onready var screen_effect_material: ShaderMaterial = get_node("Camera3D/ScreenEffects").mesh.material

@onready var environment: Environment = get_parent().get_node("WorldEnvironment").environment
@onready var main := get_parent().get_parent()
@onready var time_counter := main.get_node("TimeCounter")
@onready var kill_counter := main.get_node("KillCounter")

func _ready():
	update_fov()
	update_tonemap_exposure()
	Globals.fov_changed.connect(update_fov)
	Globals.tonemap_exposure_changed.connect(update_tonemap_exposure)
	time_counter.text = "00:00"
	kill_counter.text = "0"
	ambient_noise.play()

func _process(delta):
	if dead:
		return

	time_count += delta
	time_counter.text = "%02d:%02d" % [int(time_count / 60), int(time_count) % 60]

	var noise_playback := ambient_noise.get_stream_playback()
	for i in noise_playback.get_frames_available():
		var rn = randf() / 60
		noise_playback.push_frame(Vector2(rn, rn))

func _physics_process(delta: float):
	if dead:
		process_death(delta)
		return

	var phantom_distance_sq := PHANTOM_MAX_DISTANCE_SQ
	var nearest_phantom := Node3D.new()
	for phantom in phantom_effect_area.get_overlapping_bodies():
		var distance_sq := position.distance_squared_to(phantom.position)
		if  distance_sq < phantom_distance_sq:
			phantom_distance_sq = distance_sq
			nearest_phantom = phantom
	var effect_strength := inverse_lerp(PHANTOM_MAX_DISTANCE_SQ, PHANTOM_MIN_DISTANCE_SQ, phantom_distance_sq)

	screen_effect_material.set_shader_parameter("phantom_effect_strength", effect_strength)
	environment.adjustment_saturation = 1 - effect_strength
	AudioServer.set_bus_volume_db(1, -20 * effect_strength)
	if phantom_distance_sq < PHANTOM_MIN_DISTANCE_SQ:
		die(nearest_phantom)

	var look_direction := Input.get_vector("look_left", "look_right", "look_up", "look_down")
	yaw(look_direction.x * STICK_X_SENSITIVITY)
	pitch(look_direction.y * STICK_Y_SENSITIVITY)
	shadow_arms.look_at(gun.global_position)

	if is_on_floor():
		var input_direction := Input.get_vector("walk_left", "walk_right", "walk_forward", "walk_back")
		var walk_direction := Vector3(input_direction.x, 0, input_direction.y).rotated(up_direction, yaw_node.rotation.y)
		velocity = walk_direction * WALK_SPEED

		if fall_time > MAX_SILENT_FALL_TIME:
			footstep_audio.play()
		fall_time = 0

		var step_progress := delta * STEP_MAX_PROGRESS
		var next_step_phase := step_phase + step_progress
		if !velocity.is_zero_approx():
			# Walking
			step_phase = next_step_phase
		elif !is_zero_approx(sin(step_phase)):
			# Returning to standing
			if step_phase < PI:
				step_phase = min(next_step_phase, PI)
			else:
				step_phase = min(next_step_phase, TAU)
		else:
			# Standing
			step_phase = 0.0
		step_phase = fmod(step_phase, TAU)
		if abs(cos(step_phase) + 1) < step_progress:
			footstep_audio.play()
		camera.position.y = camera_height + sin(step_phase) * VIEWBOB_BASE_AMPLITUDE * Globals.viewbob_intensity
	else:
		fall_time += delta

	velocity += GRAVITY * delta
	move_and_slide()

func _input(event):
	if Input.is_action_pressed("ui_cancel"):
		main.pause()
		return

	if dead or Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return

	if event is InputEventMouseMotion:
		yaw(event.relative.x * MOUSE_X_SENSITIVITY)
		pitch(event.relative.y * MOUSE_Y_SENSITIVITY)
		return

	if event.is_action_pressed("fire") and \
			!gun_animation.is_playing() and gun_raised:
		gun_audio.play()
		gun_animation.play("Recoil")
		gunray.force_raycast_update()
		var target = gunray.get_collider()
		if target and target.has_method("get_rekt"):
			if(target.get_rekt()):
				kill_count += 1
				kill_counter.text = str(kill_count)
		return

func yaw(angle: float) -> void:
	yaw_node.rotation.y = fmod(yaw_node.rotation.y + angle, TAU)

func pitch(angle: float) -> void:
	pitch_node.rotation.x = clamp(pitch_node.rotation.x + angle, -PI/2, PI/2)

func _on_GenerateLevelArea_body_entered(body):
	body.generate_adjacent()

func _on_KeepLevelArea_body_exited(body):
	body.queue_free()

func check_gun_area(body):
	if body == self:
		return
	if gun_raised and gun_area.get_overlapping_bodies().size() > 0:
		gun_raised = false
		gun_animation.play("LowerGun")
	elif !gun_raised:
		gun_raised = true
		gun_animation.play_backwards("LowerGun")

func die(killer: Node3D) -> void:
	if Globals.immortal or dead:
		return
	dead = true
	velocity = Vector3.ZERO
	environment.adjustment_brightness = DEATH_BRIGHTNESS
	screen_effect_material.set_shader_parameter("phantom_effect_strength", 0)

	shadow_body.hide()
	shadow_arms.hide()
	gun.hide()
	var dropped_gun := preload("res://Player/Gun.tscn").instantiate()
	dropped_gun.transform = gun.global_transform
	add_sibling(dropped_gun)

	camera.look_at(killer.position + Vector3(0, camera_height, 0))

	var timer := Timer.new()
	timer.one_shot = true
	timer.timeout.connect(start_rapture)
	add_child(timer)
	timer.start(STARE_TIME)

func start_rapture() -> void:
	rapture_started = true
	var timer := Timer.new()
	timer.one_shot = true
	timer.timeout.connect(main.show_death_screen)
	add_child(timer)
	timer.start(RAPTURE_TIME)

func process_death(delta: float) -> void:
	if rapture_started:
		pitch_node.rotation.x = max(pitch_node.rotation.x - RAPTURE_PITCH_SPEED, -PI/2)
		position.y += delta * RAPTURE_SPEED

func update_fov() -> void:
	camera.fov = Globals.fov

func update_tonemap_exposure() -> void:
	environment.tonemap_exposure = Globals.tonemap_exposure
