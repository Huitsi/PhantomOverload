<!--
SPDX-FileCopyrightText: 2022-2024 Linus Vanas
SPDX-License-Identifier: GPL-3.0-or-later
-->
# Phantom Overload

![Screenshot of the city world](screenshots/city.png)
![Screenshot of the forest world](screenshots/forest.png)

Phantom Overload is an atmospheric first person shooter game where you
hunt – and are hunted by – phantoms in a procedurally generated world.
As of April 2024 the game is playable, though very much a work in progress.
It's being made with [Godot](https://godotengine.org) 4.

## Links
* [Homepage](https://huitsi.net/PhantomOverload)
* [Releases on Codeberg](https://codeberg.org/Huitsi/PhantomOverload/releases)
* [Project on Codeberg](https://codeberg.org/Huitsi/PhantomOverload)
